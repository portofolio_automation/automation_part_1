<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button_hover_login</name>
   <tag></tag>
   <elementGuidId>54ea1bd1-4a85-4bfc-91cf-0a342e6537ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[text()='Log In']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Log In']</value>
      <webElementGuid>ce764d44-53b4-44bc-9d5c-52934d6eee5c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
