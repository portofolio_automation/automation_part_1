<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Password_field</name>
   <tag></tag>
   <elementGuidId>c47a82dd-e80f-43e7-aec5-172d1a7febb0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type='password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@type='password']</value>
      <webElementGuid>b8651c90-a7c8-4399-bc4b-496f37d073be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
