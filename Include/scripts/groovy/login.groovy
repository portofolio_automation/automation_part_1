import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class login {
	/**
	 *     Given User open browser
    When user go to login page and user click login to open
    And user will input <email> and <password>
    And user click button login
    Then user will be redirict to dashboard shopee
	 */
	@Given("User open browser")
	def User_open_browser () {
		WebUI.callTestCase(findTestCase('Component/Default/Open_Browser'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user go to login page and user click login to open")
	def user_go_to_login_page_and_user_click_login_to_page (){
		WebUI.callTestCase(findTestCase('Component/Login/Button_hover_login'), [:], FailureHandling.STOP_ON_FAILURE)

	}
	@When ("user will input (.*) and (.*)")
	def User_will_input_email_and_password (String email, String password) {
		WebUI.callTestCase(findTestCase('Component/Login/Email_field'), [email:(email)], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Component/Login/Password_field'), [password: (password)], FailureHandling.STOP_ON_FAILURE)
	}
	@When ("user click button login")
	def user_click_button_login () {
		WebUI.callTestCase(findTestCase('Component/Login/Button_Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will be redirict to dashboard shopee")
	def user_will_be_redirict_to_dashboard_shopee () {
		println status
	}
}